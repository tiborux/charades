    package es.tiborux.charades.app;
    import android.content.Context;
    import android.content.Intent;
    import android.content.res.Resources;
    import android.graphics.Color;
    import android.hardware.Sensor;
    import android.hardware.SensorEvent;
    import android.hardware.SensorEventListener;
    import android.hardware.SensorManager;
    import android.media.AudioManager;
    import android.media.MediaPlayer;
    import android.media.SoundPool;
    import android.os.Handler;
    import android.support.v7.app.ActionBarActivity;
    import android.os.Bundle;
    import android.util.Log;
    import android.view.Menu;
    import android.view.MenuItem;
    import android.view.View;
    import android.view.Window;
    import android.view.WindowManager;
    import android.widget.RelativeLayout;
    import android.widget.TextView;

    import java.util.ArrayList;


    public class JuegoActivity extends ActionBarActivity implements SensorEventListener,View.OnClickListener {
        private SensorManager sensorManager;

        private long lastUpdate;
        public static String categorias[];
        public static TextView solucion;
        public static TextView preparado;
        public static TextView comenzar;
        public static TextView cuenta_atras;
        public static int contador;
        public static RelativeLayout juego_layout;
        public static final String NOMBRE = "es.tiborux.charades.app.NOMBRE";
        public static Resources res;
        private final static int CLOCK_UPDATE_PERIOD_MS = 1000; // 1 s
        private final static int DEFAULT_GAME_DURATION_MS = 60 * 1000; // 1 min
        private final static int DEFAULT_GAME_COUNTDOWN = 4 * 1000; // 5 s
        private boolean mIsPlaying = false;
        public static final int RANDOM = 6;
        private int valorInicial=0;
        private int valorFinal;
        private Handler mTimerHandler = new Handler();
        private Runnable mUpdateTimeTask = new TimeUpdater();
        private TextView mClock;
        private long mDurationMs;
        private long mStartMs;
        private boolean cuentaAtras;
        private int wrong_sound,correct_sound;
        private final Context context = this;
        private ArrayList<Integer> listaNumero = new ArrayList<Integer>();
        private SoundPool soundPool;
        private MediaPlayer time_sound;
        private ArrayList<String> puntuaciones;
        private ArrayList<Integer> aciertos_fallos;
        private int aciertos,preguntas_totales;


        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
            setContentView(R.layout.activity_juego);
            //Generamos un numero aleatorio
            contador = generar();
            //Inicializamos los sonidos
            soundPool = new SoundPool( 5, AudioManager.STREAM_MUSIC , 0);
            time_sound = MediaPlayer.create(this,R.raw.clock);
            wrong_sound  =soundPool.load(context, R.raw.wrong,0);
            correct_sound = soundPool.load(context, R.raw.correct,0);
            //Le damos el valor true para elegir que estamos jugando
            mIsPlaying = true;
            Intent intent = getIntent();
            //Recuperamos el valor que pasamos con el intent
            categorias = intent.getStringArrayExtra(NOMBRE);
            //Inicializamos los sensores
            sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
            //Inicializamos el temporizador
            lastUpdate = System.currentTimeMillis();
            res = getResources();
            //Inicializamos las vistas y listener
            solucion = (TextView) findViewById(R.id.solucion);
            juego_layout = (RelativeLayout) findViewById(R.id.layout_juego);
            juego_layout.setOnClickListener(this);
            preparado = (TextView) findViewById(R.id.preparado);
           comenzar = (TextView) findViewById(R.id.comenzar);
            //Inicializamos dos arraylist para guardar los aciertos y fallos, y todas las preguntas que van saliendo
            puntuaciones=new ArrayList<String>();
            aciertos_fallos=new ArrayList<Integer>();
            valorFinal=categorias.length;
            //Fijamos en la pregunta actual, una aleatoria de la elegida
            solucion.setText(categorias[contador]);
            //Inicializamos los aciertos y preguntas_totales
            aciertos=0;
            preguntas_totales=0;
        }


        @Override
        protected void onStop() {
            super.onStop();
            //Paramos los sonidos
            time_sound.stop();
            //Vaciamos la lista que contiene todos los resultados posibles del metodo generar()
            listaNumero.clear();
            //Paramos el sensor
            sensorManager.unregisterListener(this);
        }

        @Override
        public boolean onCreateOptionsMenu(Menu menu) {

            // Inflate the menu; this adds items to the action bar if it is present.
            getMenuInflater().inflate(R.menu.juego, menu);
            return true;
        }

        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            // Handle action bar item clicks here. The action bar will
            // automatically handle clicks on the Home/Up button, so long
            // as you specify a parent activity in AndroidManifest.xml.
            int id = item.getItemId();
            if (id == R.id.action_settings) {
                return true;
            }
            return super.onOptionsItemSelected(item);
        }

        @Override
        public void onSensorChanged(SensorEvent event) {
                if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
                    getAccelerometer(event);
                }

            }



        private void getAccelerometer(SensorEvent event) {
        float x = event.values[0];
        float y = event.values[1];
        float z = event.values[2];
        //Cogemos la aceleracion
        float accelationSquareRoot = (x * x + y * y + z * z)
                / (SensorManager.GRAVITY_EARTH * SensorManager.GRAVITY_EARTH);
        //Cogemos el tiempo actual
        long actualTime = System.currentTimeMillis();
        if (accelationSquareRoot >= 2) {
            if (actualTime - lastUpdate < 200) {
                return;
            }
            lastUpdate = actualTime;
            if (z < -6) {
                //SI se inclina el movil hacia adelante
                //Fijamos el background de color verde
                    juego_layout.setBackgroundColor(Color.parseColor(res.getString(R.string.correct_color)));
                //Reproducimos el sonido de correcto
                    soundPool.play(correct_sound, 1, 1, 1, 0, 1);
                //Cambiamos el texto por la palabra "Correcto"
                    solucion.setText(getResources().getString(R.string.correct));
                    juego_layout.invalidate();
                //Añadimos al arraylist la palabra que acabamos de acertar
                    puntuaciones.add(categorias[contador]);
                //Generamos de nuevo un número aleatorio
                    contador=generar();
                //Añadimos un 1 para indicar que era una pregunta correcta
                    aciertos_fallos.add(1);
                //Llamamos a la funcion aciertaFallaPregunta
                    AciertaFallaPregunta();
                //Aumentamos los aciertos y las preguntas totales
                    aciertos++;
                    preguntas_totales++;



            } else if (z > -4) {
                //Fijamos el background de color rojo
                    juego_layout.setBackgroundColor(Color.parseColor(res.getString(R.string.pass_color)));
                //Reproducimos el sonido de incorrecto
                    soundPool.play(wrong_sound, 1, 1, 1, 0, 1);
                //Cambiamos el texto por "Pasar"
                    solucion.setText(getResources().getString(R.string.pass));
                    juego_layout.invalidate();
                //Llamamos a la funcion aciertaFallaPregunta
                    AciertaFallaPregunta();
                //Añadimos al arraylist la palabra que acabamos de acertar
                    puntuaciones.add(categorias[contador]);
                //Generamos un numero aleatorio
                    contador=generar();
                //Añadimos un 0 para indicar que era una pregunta incorrecta
                    aciertos_fallos.add(0);
                //Aumentamos las preguntas totales
                    preguntas_totales++;


            }
        }

        }


        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {

        }

        @Override
        protected void onResume() {
            super.onResume();



        }

        @Override
        protected void onPause() {

            super.onPause();
            //Paramos los sensores y los sonidos, y reseteamos el temporizador
            sensorManager.unregisterListener(this);
            time_sound.stop();
            mTimerHandler.removeCallbacks(mUpdateTimeTask);
        }

        public static void AciertaFallaPregunta() {
            //Creamos un handler para que espere 1 segundo, cuando pasa el tiempo vuelve a poner el color normal y pone una nueva palabra
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                public void run() {
                    juego_layout.setBackgroundColor(Color.parseColor(res.getString(R.string.background_color)));
                    solucion.setText(categorias[contador]);
                }
            }, 1000);

        }

        @Override
        public void onClick(View v) {
            //Llamamos  a la función cuenta atrás
           cuentaAtras();

        }

        private final class TimeUpdater implements Runnable {
            public void run() {
                //Si cuentaAtras es verdad
                if(cuentaAtras)
                {
                    //El reloj comienza desde 3
                    mClock=(TextView) findViewById(R.id.cuenta_atras);
                    mClock.setVisibility(View.VISIBLE);
                }
                //Iniciamos el sonido de reloj
                time_sound.start();
                mTimerHandler.removeCallbacks(mUpdateTimeTask);

                long nowMs = System.currentTimeMillis();
                long remainingTimeMs = mDurationMs - (nowMs - mStartMs);
                int remainingTimeSec = (int) (remainingTimeMs / 1000);
                int remainingTimeMin = remainingTimeSec / 60;
                remainingTimeSec = remainingTimeSec % 60;

                final long tenSecondsInMs = 10000;
                //Dependiendo del tiempo que quede se ponen de un color o otro los segundos
                if ((remainingTimeMs < tenSecondsInMs)) {
                    if(cuentaAtras) {
                        mClock.setTextColor(Color.parseColor("#E6E6E6"));
                    }
                    else {
                        mClock.setTextColor(Color.RED);
                    }
                } else {
                    mClock.setTextColor(Color.parseColor("#E6E6E6"));
                }
                //Si estamos en cuenta atras, solo se muestra 1 digito
                if(cuentaAtras){
                    mClock.setText(String.format("%01d",
                            remainingTimeSec));
                }
               else {
                //Si no, mostramos dos digitos
                    mClock.setText(String.format("%02d",
                            remainingTimeSec));
                }

                if (remainingTimeMin > 0 || remainingTimeSec > 0) {

                    mTimerHandler.postDelayed(mUpdateTimeTask,
                            CLOCK_UPDATE_PERIOD_MS);


                } else {
                    if(cuentaAtras) {
                        //Cuando acaba la cuenta atras, quitamos el reloj y ponemos el nuevo reloj para jugar
                        cuentaAtras=false;
                        mClock.setVisibility(View.GONE);
                        mClock=(TextView) findViewById(R.id.text_view_clock);
                        mClock.setVisibility(View.VISIBLE);
                    //Llamamos a la funcion startGame
                        startGame();
                    }
                    else{
                        //Cuando se acaba el tiempo del juego pasamos las preguntas totales, las acertadas, las corretas e incorrectes y todas las palabras que han salido
                        //y se las pasamos a la nueva actividad
                        Intent intent = new Intent(JuegoActivity.this, PuntuacionesActivity.class);
                        intent.putExtra(PuntuacionesActivity.PUNTUACIONES,puntuaciones);
                        intent.putExtra(PuntuacionesActivity.ACIERTOS_FALLOS,aciertos_fallos);
                        intent.putExtra(PuntuacionesActivity.ACIERTOS, aciertos);
                        intent.putExtra(PuntuacionesActivity.PREGUNTAS_TOTALES,preguntas_totales);
                        startActivity(intent);

                    }

                }
            }

        }
        void setGameDuration(long durationMs) {
            mDurationMs = durationMs;
        }

        void startGame() {
    //Cuando empieza el juego iniciamos el sensor
            sensorManager.registerListener(this,
                    sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                    SensorManager.SENSOR_DELAY_NORMAL);
            //Adecuamos el layout
            preparado.setVisibility(View.GONE);
            comenzar.setVisibility(View.GONE);
            mClock.setVisibility(View.VISIBLE);
            solucion.setText(categorias[contador]);
            solucion.setVisibility(View.VISIBLE);
            mIsPlaying = true;
            mStartMs = System.currentTimeMillis();
            this.supportInvalidateOptionsMenu();

            final long updateTime = 0; // Hacer la primera actualización
            // inmediatamente
            mDurationMs=DEFAULT_GAME_DURATION_MS;
            mTimerHandler.postDelayed(mUpdateTimeTask, updateTime);
        }

        void cuentaAtras(){
            //Comienza la cuenta atras para iniciar el juego
            cuentaAtras=true;
            juego_layout.setClickable(false);
            preparado.setVisibility(View.GONE);
            comenzar.setVisibility(View.GONE);
            mStartMs = System.currentTimeMillis();
            this.supportInvalidateOptionsMenu();

            final long updateTime = 0; // Hacer la primera actualización
            // inmediatamente
            mDurationMs=DEFAULT_GAME_COUNTDOWN;
            mTimerHandler.postDelayed(mUpdateTimeTask, updateTime);

        }

        //Metodo para generar numeros aleatorios sin repetir numeros.
        public int generar(){
            if(listaNumero.size() < (valorFinal - valorInicial) +1){
                //Aun no se han generado todos los numeros
                int numero = numeroAleatorio();//genero un numero
                if(listaNumero.isEmpty()){//si la lista esta vacia
                    listaNumero.add(numero);
                    return numero;
                }else{//si no esta vacia
                    if(listaNumero.contains(numero)){//Si el numero que generé esta contenido en la lista
                        return generar();//recursivamente lo mando a generar otra vez
                    }else{//Si no esta contenido en la lista
                        listaNumero.add(numero);
                        return numero;
                    }
                }
            }else{// ya se generaron todos los numeros
                return -1;
            }
        }


//Metodo para generar numeros aleatorios repitiendo
        private int numeroAleatorio(){

            return (int)(Math.random()*(valorFinal-valorInicial+1)+valorInicial);

        }

    }
