package es.tiborux.charades.app;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;


public class PuntuacionesActivity extends ActionBarActivity implements View.OnClickListener {

    public static final String PUNTUACIONES = "es.tiborux.charades.app.PUNTUACIONES";
    public static final String ACIERTOS_FALLOS = "es.tiborux.charades.app.ACIERTOS_FALLOS";
    public static final String ACIERTOS = "es.tiborux.charades.app.ACIERTOS";
    public static final String PREGUNTAS_TOTALES = "es.tiborux.charades.app.PREGUNTAS_TOTALES";
    private ArrayList<String> puntuaciones;
    private ArrayList<Integer> aciertos_fallos;
    private int aciertos,preguntas_totales;
    private ImageView back;
    private TextView puntuacion_total;
    private ListView mLista;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Quitamos la action bar y ponemos la activity en pantalla completa
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_puntuaciones);
        //Buscamos la view de la lista de puntuaciones
        mLista = (ListView) findViewById(R.id.lv_listado);
        //Inicializamos las ArrayList
        puntuaciones   =new ArrayList<String>();
        aciertos_fallos=new ArrayList<Integer>();
        //Inicializamos un intent
        Intent intent = getIntent();
        //Cogemos del intent los parametros pasados por la activity anterior
        //Puntuaciones: Contiene las preguntas acertadas de la activity juego
        puntuaciones = intent.getStringArrayListExtra(PUNTUACIONES);
        //Aciertos_fallos: contiene un 0 cuando se falla y un 1 cuando se acierta, para que a la hora de mostrarlas sepas cuales estan mal y cualesbien
        aciertos_fallos = intent.getIntegerArrayListExtra(ACIERTOS_FALLOS);
        //Aciertos: contiene el número de preguntas acertadas
        aciertos=intent.getIntExtra(ACIERTOS, 0);
        //Preguntas_totales: contiene el número de preguntas totales
        preguntas_totales=intent.getIntExtra(PREGUNTAS_TOTALES, 0);
        //Iniciamos la vista de la imagen Back y le asociamos un Listener
        back=(ImageView) findViewById(R.id.back);
        back.setOnClickListener(this);
        //Creamos el adapter y se lo fijamos a la lista
        PuntuacionesAdaptar adapter=new PuntuacionesAdaptar(this,puntuaciones,aciertos_fallos);
        mLista.setAdapter(adapter);
        //Iniciamos la vista del textView y escribimos en el las preguntas acertadas y el total de preguntas
        puntuacion_total=(TextView) findViewById(R.id.puntuacion_total);
        puntuacion_total.setText(aciertos+"/"+preguntas_totales);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.puntuaciones, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            //Cuando pulsemos la imagen de back volveremos al menú de categorias
            case R.id.back:
                Intent intent=new Intent(PuntuacionesActivity.this, CategoriasActivity.class);
                startActivity(intent);
                break;
        }
    }
}
