package es.tiborux.charades.app;

/**
 * Clase que adapta los datos obtenidos en el RSS de Agenda y Noticias y los traslada al
 * layout correspondiente.
 */

        import java.util.ArrayList;
        import android.app.Activity;
        import android.content.Context;
        import android.text.Html;
        import android.text.Spanned;
        import android.view.LayoutInflater;
        import android.view.View;
        import android.view.ViewGroup;
        import android.widget.BaseAdapter;
        import android.widget.ImageView;
        import android.widget.TextView;

public class PuntuacionesAdaptar extends BaseAdapter{

    // Creación de variables
    protected Activity activity;
    protected ArrayList<String> items;
    protected ArrayList<Integer> comprobar;

    //Constructor
    public PuntuacionesAdaptar(Activity activity, ArrayList<String> items,ArrayList<Integer> comprobar ) {
        this.activity = activity;
        this.items = items;
        this.comprobar=comprobar;
    }

    //Devuelve el número de elementos de la lista
    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        // Generamos una convertView por motivos de eficiencia
        View v = convertView;

        //Asociamos el layout de la lista que hemos creado
        if(convertView == null){
            LayoutInflater inf = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inf.inflate(R.layout.rellenar_puntuaciones, null);
        }


        Context context = parent.getContext();

        //Rellenamos el titulo
        TextView resultado = (TextView) v.findViewById(R.id.aciertos_fallos_puntuaciones);
        ImageView imagen= (ImageView)v.findViewById(R.id.comprobar);
        //Si es incorrecto
        if(comprobar.get(position)==0) {
            resultado.setText(items.get(position));
            imagen.setImageResource(R.drawable.incorrect);
        }
        else {
            resultado.setText(items.get(position));
            imagen.setImageResource(R.drawable.correct);
        }


        // Retornamos la vista
        return v;
    }
}