package es.tiborux.charades.app;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;


public class CategoriasActivity extends ActionBarActivity implements View.OnClickListener{
    ImageView categoria_animales,categoria_celebrities,categoria_peliculas,categoria_deportes,categoria_television,categoria_videojuegos,categoria_comida,categoria_marcas;
    Intent  intent;
    String categorias[];
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_categorias);
        //Inicializamos vistas y listener
        categoria_animales = (ImageView) findViewById(R.id.categoria_animales);
        categoria_animales.setOnClickListener(this);
        categoria_peliculas = (ImageView) findViewById(R.id.categoria_peliculas);
        categoria_peliculas.setOnClickListener(this);
        categoria_celebrities = (ImageView) findViewById(R.id.categoria_celebrities);
        categoria_celebrities.setOnClickListener(this);
        categoria_deportes = (ImageView) findViewById(R.id.categoria_deportes);
        categoria_deportes.setOnClickListener(this);
        categoria_television = (ImageView) findViewById(R.id.categoria_television);
        categoria_television.setOnClickListener(this);
        categoria_videojuegos = (ImageView) findViewById(R.id.categoria_videojuegos);
        categoria_videojuegos.setOnClickListener(this);
        categoria_comida = (ImageView) findViewById(R.id.categoria_comida);
        categoria_comida.setOnClickListener(this);
        categoria_marcas = (ImageView) findViewById(R.id.categoria_marcas);
        categoria_marcas.setOnClickListener(this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.categorias, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    public void onClick(View v) {
        /*Comprobamos a que id pertenece el id del botón pulsado, en este caso como solo tenemos 1
        siempre va a ser el mismo pero se puede dar el caso de que haya más de un botón, entonces aquí se controlaría que botón se ha pulsado.
        */
        switch(v.getId()){
    //Dependiendo la categoria que elijamos, se le pasa a la siguiente actividad un array con una categoria o con otra
            case R.id.categoria_animales:
                //Si el id del botón pulsado se corresponde con el id que tenemos aquí entraría dentro del case.
                //Aquí va el código que se va a ejecutar cuando se pulse el botón.
                /*Creamos una nueva actividad y le pasamos el array de animales*/
                intent = new Intent(this, JuegoActivity.class);
                categorias=getResources().getStringArray(R.array.array_animales);
                intent.putExtra(JuegoActivity.NOMBRE,categorias);
                startActivity(intent);
                break;
            case R.id.categoria_peliculas:
                //Si el id del botón pulsado se corresponde con el id que tenemos aquí entraría dentro del case.
                //Aquí va el código que se va a ejecutar cuando se pulse el botón.
                /*Creamos una nueva actividad y le pasamos el array de peliculas*/
                intent = new Intent(this, JuegoActivity.class);
                categorias=getResources().getStringArray(R.array.array_peliculas);
                intent.putExtra(JuegoActivity.NOMBRE,categorias);
                startActivity(intent);
                break;
            case R.id.categoria_celebrities:
                //Si el id del botón pulsado se corresponde con el id que tenemos aquí entraría dentro del case.
                //Aquí va el código que se va a ejecutar cuando se pulse el botón.
                /*Creamos una nueva actividad y le pasamos el array de animales*/
                intent = new Intent(this, JuegoActivity.class);
                categorias=getResources().getStringArray(R.array.array_celebrities);
                intent.putExtra(JuegoActivity.NOMBRE,categorias);
                startActivity(intent);
                break;
            case R.id.categoria_deportes:
                //Si el id del botón pulsado se corresponde con el id que tenemos aquí entraría dentro del case.
                //Aquí va el código que se va a ejecutar cuando se pulse el botón.
                /*Creamos una nueva actividad y le pasamos el array de peliculas*/
                intent = new Intent(this, JuegoActivity.class);
                categorias=getResources().getStringArray(R.array.array_deportes);
                intent.putExtra(JuegoActivity.NOMBRE,categorias);
                startActivity(intent);
                break;
            case R.id.categoria_television:
                //Si el id del botón pulsado se corresponde con el id que tenemos aquí entraría dentro del case.
                //Aquí va el código que se va a ejecutar cuando se pulse el botón.
                /*Creamos una nueva actividad y le pasamos el array de animales*/
                intent = new Intent(this, JuegoActivity.class);
                categorias=getResources().getStringArray(R.array.array_television);
                intent.putExtra(JuegoActivity.NOMBRE,categorias);
                startActivity(intent);
                break;
            case R.id.categoria_videojuegos:
                //Si el id del botón pulsado se corresponde con el id que tenemos aquí entraría dentro del case.
                //Aquí va el código que se va a ejecutar cuando se pulse el botón.
                /*Creamos una nueva actividad y le pasamos el array de animales*/
                intent = new Intent(this, JuegoActivity.class);
                categorias=getResources().getStringArray(R.array.array_videojuegos);
                intent.putExtra(JuegoActivity.NOMBRE,categorias);
                startActivity(intent);
                break;
            case R.id.categoria_comida:
                //Si el id del botón pulsado se corresponde con el id que tenemos aquí entraría dentro del case.
                //Aquí va el código que se va a ejecutar cuando se pulse el botón.
                /*Creamos una nueva actividad y le pasamos el array de animales*/
                intent = new Intent(this, JuegoActivity.class);
                categorias=getResources().getStringArray(R.array.array_comida);
                intent.putExtra(JuegoActivity.NOMBRE,categorias);
                startActivity(intent);
                break;
            case R.id.categoria_marcas:
                //Si el id del botón pulsado se corresponde con el id que tenemos aquí entraría dentro del case.
                //Aquí va el código que se va a ejecutar cuando se pulse el botón.
                /*Creamos una nueva actividad y le pasamos el array de animales*/
                intent = new Intent(this, JuegoActivity.class);
                categorias=getResources().getStringArray(R.array.array_marcas);
                intent.putExtra(JuegoActivity.NOMBRE,categorias);
                startActivity(intent);
                break;


        }
    }


}
