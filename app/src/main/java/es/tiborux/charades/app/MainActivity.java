package es.tiborux.charades.app;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.media.AudioManager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;



public class MainActivity extends ActionBarActivity implements View.OnClickListener {


    private ImageView imagen_jugar;
    private final Context context = this;
    private ImageView imagen_how_to_play;
    private ImageView imagen_opciones;
    private Dialog dialog;
    private ImageView imagen_close,imagen_sound,imagen_language,imagen_time;
    public boolean on_true=true,idioma_true=true;
    AudioManager amanager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);
        //Inicializamos las vistas y los listener
        imagen_jugar = (ImageView) findViewById(R.id.playbutton);
        imagen_jugar.setOnClickListener(this);
        imagen_how_to_play = (ImageView) findViewById(R.id.howtoplaybuttom);
        imagen_how_to_play.setOnClickListener(this);
        imagen_opciones = (ImageView) findViewById(R.id.optionsbuttom);
        imagen_opciones.setOnClickListener(this);
        amanager=(AudioManager)getSystemService(Context.AUDIO_SERVICE);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
//Si el id del botón pulsado se corresponde con el id que tenemos aquí entraría dentro del case.
            //Aquí va el código que se va a ejecutar cuando se pulse el botón.
            case R.id.playbutton:
                //Si pulsamos el boton de play, cambiamos a la activity categorias
                Intent intent = new Intent(MainActivity.this, CategoriasActivity.class);
                startActivity(intent);
                break;

            case R.id.howtoplaybuttom:
                //Si pulsamos el boton de como jugar, aparece un dialog
                howToPlay();
                break;
            case R.id.optionsbuttom:
                //Si pulsamos el boton de opciones, aparece un dialog
                options();
                break;
            case R.id.idioma:
                if(idioma_true) {
                    imagen_language.setImageResource(R.drawable.espanol_ingles);
                    idioma_true=false;
                }
                else {imagen_language.setImageResource(R.drawable.ingles_espanol);
                    idioma_true=true;}
                break;
            case R.id.tiempo:
                break;
            case R.id.sound_on_off:
                if(on_true) {
                    imagen_sound.setImageResource(R.drawable.off_on);
                    amanager.setStreamMute(AudioManager.STREAM_MUSIC, true);
                    on_true=false;
                }
                else {imagen_sound.setImageResource(R.drawable.on_off);
                    amanager.setStreamMute(AudioManager.STREAM_MUSIC, false);
                on_true=true;}
                break;
        }


    }

    public void howToPlay() {
    //Le pasamos a la funcion dialgo el id del layout viewflipper
       dialog(R.layout.how_to_play1);

    }

    public void options() {
    //Le pasamos a la funcion dialgo el id del layout opciones

        dialog(R.layout.opciones);


    }
    public void dialog(int v){
        //Se crea el dialog y le quitamos el titulo
        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        //Le ponemos de layout el que recibe la función
        dialog.setContentView(v);
        dialog.show();
        //Fijamos el tamaño del dialog para que se adapte a la pantalla
        Display display = ((WindowManager) getSystemService(context.WINDOW_SERVICE)).getDefaultDisplay();
        int width = display.getWidth();
        int height = display.getHeight();
        dialog.getWindow().setLayout((6 * width) / 6, (5 * height) / 5);
        //Inicializamos la la vista y los listener de la imagen de cerrar
        imagen_close = (ImageView) dialog.findViewById(R.id.close);

        imagen_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                //Cuando pulsamos la imagen de cerrar, cerramos el dialog
                    dialog.dismiss();
                }

        });

    }


}